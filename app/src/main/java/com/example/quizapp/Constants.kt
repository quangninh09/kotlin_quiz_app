package com.example.quizapp

object Constants {

    fun getQuestions(): ArrayList<Question> {
        val questionList = ArrayList<Question>()

        val que1 = Question(
            1,
            "What country does this flag belong to?",
            R.drawable.argentina,
            "111111",
            "Australia",
            "Armenia",
            "Australia",
            1
        )
        val que2 = Question(
            1,
            "What country does this flag belong to?",
            R.drawable.argentina,
            "22222",
            "Australia",
            "Armenia",
            "Australia",
            2
        )
        val que3 = Question(
            1,
            "What country does this flag belong to?",
            R.drawable.argentina,
            "33333",
            "Australia",
            "Armenia",
            "Australia",
            3
        )

        val que4 = Question(
            1,
            "What country does this flag belong to?",
            R.drawable.argentina,
            "44444",
            "Australia",
            "Armenia",
            "Australia",
            3
        )

        questionList.add(que1)
        questionList.add(que2)
        questionList.add(que3)
        questionList.add(que4)

        return questionList
    }
}